import Joi from '@hapi/joi';

const schemas = {
  createPost: Joi.object({
    idUser: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
    content: Joi.string().required(),
    uploadimg: Joi.binary(),
  }),
  updatePost: Joi.object({
    content: Joi.string().required(),
    uploadimg: Joi.binary(),
  }),
  comment: Joi.object({
    idPost: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
    content: Joi.string().required(),
  }),
  reaction: Joi.object({
    idUser: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
    idPost: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
  }),
  postID: Joi.object({
    id: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
  }),
  userID: Joi.object({
    id: Joi.string()
      .guid({ version: 'uuidv4' })
      .required(),
  }),
};

export default schemas;
