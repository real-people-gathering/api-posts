import mult from 'multer';
import multerS3 from 'multer-s3';
import aws from 'aws-sdk';
import uuid from 'uuid/v4';
import config from '../config';

const s3 = new aws.S3({
  accessKeyId: config.bucket.accessKey,
  secretAccessKey: config.bucket.secretKey,
  endpoint: config.bucket.endpoint,
});

const storage = multerS3({
  s3,
  bucket: config.bucket.name,
  contentType: multerS3.AUTO_CONTENT_TYPE,
  key(req, file, cb) {
    const ext = file.originalname.split('.').pop();
    cb(null, uuid() + '.' + ext); // Upload file with an uuid + extension
  },
  acl: 'public-read', // Everyone can see the file
});

const upload = mult({
  storage,
});

export default { upload };
