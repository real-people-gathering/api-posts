# Posts API

API that manages RPG posts.

Before starting the server, copy .env.default and rename it to .env !\
Then fill the environment variables inside with your own values.
